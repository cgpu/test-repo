#!/usr/bin/env nextflow

ch_action = Channel.from('Look at', 'Check', 'Stop')

process blank_this {
  container 'quay.io/lifebitai/coloc:v5.1.0'

  input:
  val(x) from ch_action
  
  script:
  """
  echo '$x this!'
  """
}